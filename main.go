package main

import (
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/SevereCloud/vksdk/v2/api"
	"github.com/SevereCloud/vksdk/v2/longpoll-user"
	"github.com/SevereCloud/vksdk/v2/longpoll-user/v3"
)

func main() {
	log.Println("бебра стартинг!!!")
    content, err := ioutil.ReadFile("blacklist.txt")
	if err != nil {
		log.Printf("Ошибка при открытии файла (%s)\n", err)
	}
	blocklist := strings.Split(strings.TrimRight(string(content), "\n"), ",")
	log.Println("блоклист:", blocklist)
	vk := api.NewVK(os.Getenv("VK_TOKEN"))
	mode := longpoll.ReceiveAttachments + longpoll.ExtendedEvents
	lp, err := longpoll.NewLongPoll(vk, mode)
	if err != nil {
		log.Fatal(err)
	}

	w := wrapper.NewWrapper(lp)

	w.OnNewMessage(func(m wrapper.NewMessage) {
		for _, id := range blocklist {
			if id == m.AdditionalData.From {
				vk.MessagesDelete(api.Params{"message_ids": m.MessageID})
				log.Printf("удалил сообщение от айди %s =(\n", id)
				}
		}
	})

	if err := lp.Run(); err != nil {
		log.Fatal(err)
	}
}
